#shiro-spring-redis

这是一个shiro、spring mvc、freemarker、redis整合的项目。帮助大家快速搭建基于shiro的web集群项目。

主要功能：
===========

shiro自有的功能我就不再复述了，请查看[shiro参考文档](http://shiro.apache.org/reference.html)。

* session共享。使得应用可以多实例之间共享session信息;
* cache共享;
* 密码加密，动态salt。每个密码都使用随机的salt来加密，使得密码更安全，就算密码被撞库或泄露了，我也不怕不怕啦~;
* remember me。本项目使用的是默认的实现，会生成一个很长的value。这个value是subject对称加密后的内容，所以想靠其他对称加密算法来缩短value的长度是不可能的。我暂时想到的缩短value的方案是，把subject持久化到redis或mongodb等快速插入查询的数据库里，然后把数据库记录的key或id赋值给remember me的value。因此当用户访问web时就把redis的key或者mongodb的id当做remember me的value传到后台了，再用key或id把subject信息查出来还不简单么;
* 整合freemarker;
* 同一账号登陆用户数限制(重复登陆限制);
* SSL支持
* 还有更多...


问题及建议
===========

hh.suse@gmail.com